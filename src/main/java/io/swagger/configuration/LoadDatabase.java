package io.swagger.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.switch_api.model.User;
import io.swagger.switch_api.model.UserRepository;
import io.swagger.switch_api.model.User.CitizenshipEnum;

@Configuration
class LoadDatabase {

  private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

  @Bean
  CommandLineRunner initDatabase(UserRepository repository) {

    //CitizenshipEnum cit1 = new
	User user1 = new User("Bartosz", "Jarosiewicz", 25, "96052212345", "test@gmail.com");
	user1.setCitizenship(CitizenshipEnum.PL);
	User user2 = new User("qwe", "asdf", 22, "22222212345", "tes12234t@zut.edu.pl");
	user2.setCitizenship(CitizenshipEnum.DE);
    return args -> {
      log.info("Preloading " + repository.save(user1));
      log.info("Preloading " + repository.save(user2));
    };
  }
}