package io.swagger.switch_api.api;

import io.swagger.switch_api.model.CreateRequest;
import io.swagger.switch_api.model.Error;
import java.util.UUID;
import io.swagger.switch_api.model.UpdateRequest;
import io.swagger.switch_api.model.User;
import io.swagger.switch_api.model.UserListResponse;
import io.swagger.switch_api.model.UserResponse;

import io.swagger.switch_api.model.UserRepository;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.swagger.Swagger2SpringBoot;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-04-13T18:23:05.437+02:00")

@Controller
public class UsersApiController implements UsersApi {

    private static final Logger log = LoggerFactory.getLogger(UsersApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public UsersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }    	
    
    @Autowired
    UserRepository repository;

    public ResponseEntity<String> createUser(@ApiParam(value = "User object that has to be added" ,required=true )  @Valid @RequestBody CreateRequest body, @ApiParam(value = "Podpis X-HMAC-SIGNATURE" ,required=true) @RequestHeader(value="X-HMAC-SIGNATURE", required=true) String X_HMAC_SIGNATURE) {
    	boolean signFlag = false;    	
    	UUID reqId = body.getRequestHeader().getRequestId();
    	
    	TimeZone tz = TimeZone.getTimeZone("GMT+2:00");
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    	df.setTimeZone(tz);
    	String nowAsISO = df.format(new Date());
    	User newUsr = body.getUser();
    	String json = new Gson().toJson(newUsr);
    	String jsonToHash = "{\n"
    			+ "  \"requestHeader\": {\n"
    			+ "    \"requestId\": \"" + body.getRequestHeader().getRequestId() + "\",\n"
    			+ "    \"sendDate\": \"" + body.getRequestHeader().getSendDate() + "\"\n"
    			+ "  },\n"
    			+ "  \"user\": " + json + "\n"
    			+ "}";

    	//System.out.print("\n"+jsonToHash);
    	try {
    		byte[] secret = "123456".getBytes("UTF-8");
        	byte[] sign = jsonToHash.getBytes("UTF-8");
		    byte[] hashed = calcHmacSha256(secret, sign);
		    String hex = String.format("%032x", new BigInteger(1, hashed));
	    	//System.out.print("hex              = " + hex + "\n");
	    	//System.out.print("X_HMAC_SIGNATURE = " + X_HMAC_SIGNATURE + "\n");
		    //Swagger2SpringBoot.key.getEncoded()
		    
	    	if(hex.equals(X_HMAC_SIGNATURE)) {
	    		signFlag = true;
	    	}
	    } catch (UnsupportedEncodingException e) {
	      e.printStackTrace();
	    }
    	if(signFlag) {
	        String accept = request.getHeader("Accept");
	    	User usr = repository.findOne(newUsr.getId());
	        if (accept != null && accept.contains("application/json")) {
	        	if(usr == null) {
		            repository.save(newUsr);
					return new ResponseEntity<String>("{  \"responseHeader\" : {    \"sendDate\" : \""+nowAsISO+"\",    \"requestId\" : \""+reqId+"\",  \"X-HMAC-SIGNATURE\" : \""+X_HMAC_SIGNATURE+"\" },  \"user\" : "+json+" }", HttpStatus.CREATED);
	        	}
	        	else {
	        		return new ResponseEntity<String>(HttpStatus.UNPROCESSABLE_ENTITY);
	        	}
	        }
    	}
    	else {
    		return new ResponseEntity<String>("{ \"code\" : \"401\", \"message\" : \"Integrity Error, signature is invalid\" ,\"responseHeader\" : {    \"sendDate\" : \""+nowAsISO+"\",    \"requestId\" : \""+reqId+"\",  \"X-HMAC-SIGNATURE\" : \""+X_HMAC_SIGNATURE+"\" } }",HttpStatus.UNAUTHORIZED);    		
    	}

        return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
    }
    
    public ResponseEntity<Void> deleteUser(@ApiParam(value = "",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
    	if (accept != null && accept.contains("application/json")) {            
	    	User usr = repository.findOne(id);
	    	if(usr != null) {
	    		repository.delete(id);
	    		return new ResponseEntity<Void>(HttpStatus.OK);
	    	}
	    	else {
	    		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);    		
	    	}
    	}
        return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST); 
    }
    
    public ResponseEntity<UserListResponse> getAllUsers() {
    	String json = new Gson().toJson(repository.findAll());
    	TimeZone tz = TimeZone.getTimeZone("GMT+2:00");
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    	df.setTimeZone(tz);
    	String nowAsISO = df.format(new Date());
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<UserListResponse>(objectMapper.readValue("{  \"usersList\" : "+json+",  \"responseHeader\" : {    \"sendDate\" : \""+nowAsISO+"\",    \"requestId\" : \"046b6c7f-0b8a-43b9-b35d-6489e6daee91\"  }}", UserListResponse.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<UserListResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<UserListResponse>(HttpStatus.BAD_REQUEST); 
    }

    public ResponseEntity<UserResponse> getUserById(@ApiParam(value = "",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
        	User usr = repository.findOne(id);
        	TimeZone tz = TimeZone.getTimeZone("GMT+2:00");
        	DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        	df.setTimeZone(tz);
        	String nowAsISO = df.format(new Date());
        	if(usr != null) {
        		try {
                	String json = new Gson().toJson(usr);
					return new ResponseEntity<UserResponse>(objectMapper.readValue("{  \"responseHeader\" : {    \"sendDate\" : \""+nowAsISO+"\",    \"requestId\" : \"046b6c7f-0b8a-43b9-b35d-6489e6daee91\"  },  \"user\" : "+json+" }", UserResponse.class), HttpStatus.OK);
				}  
        		catch (IOException e) {
	                log.error("Couldn't serialize response for content type application/json", e);
	                return new ResponseEntity<UserResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
	            }
        	}
        	else {
        		return new ResponseEntity<UserResponse>(HttpStatus.NOT_FOUND);    	
        	}
        }

        return new ResponseEntity<UserResponse>(HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<String> updateUser(@ApiParam(value = "",required=true) @PathVariable("id") Long id,@ApiParam(value = "" ,required=true )  @Valid @RequestBody UpdateRequest body, @ApiParam(value = "Podpis X-JWS-SIGNATURE" ,required=true) @RequestHeader(value="X-JWS-SIGNATURE", required=true) String X_JWS_SIGNATURE) {
    	User updatedUsr = body.getUser();
    	String json = new Gson().toJson(updatedUsr);
    	UUID reqId = body.getRequestHeader().getRequestId();    	
    	TimeZone tz = TimeZone.getTimeZone("GMT+2:00");
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    	df.setTimeZone(tz);
    	String nowAsISO = df.format(new Date());
    	String jsonToCompare = "{requestHeader:{requestId:" + body.getRequestHeader().getRequestId() + ",sendDate:" + body.getRequestHeader().getSendDate() + "},user:" + json + "}";
    	
    	try {
    	    Jws<Claims> claims = Jwts.parserBuilder().setSigningKey(Swagger2SpringBoot.key).build().parseClaimsJws(X_JWS_SIGNATURE);
    	    String parsedClaims = claims.getBody().toString().replace("=", ":");
    	    String parsedJson = jsonToCompare.replace("\"", "").replace(",", ", ");
    	    //System.out.print("parsedClaims: " + parsedClaims+"\n");
    	    //System.out.print("parsedJson  : " + parsedJson+"\n");
    	    if(parsedClaims.equals(parsedJson)) {
    	    	User usr = repository.findOne(id);    	    	
    	        String accept = request.getHeader("Accept");
    	        if (accept != null && accept.contains("application/json")) {
    	        	if(usr != null) {
    		            repository.save(updatedUsr);
						return new ResponseEntity<String>("{  \"responseHeader\" : {    \"sendDate\" : \""+nowAsISO+"\",    \"requestId\" : \""+reqId+"\"  },  \"user\" : "+json+" }", HttpStatus.OK);
    	        	}
    	        	else {
    	        		return new ResponseEntity<String>(HttpStatus.NOT_FOUND);    	
    	        	}
    	        }
    	    }
    	    else {
    	    	return new ResponseEntity<String>("{ \"code\" : \"401\", \"message\" : \"Integrity Error, User body is different than Payload data in JWS Token\" ,\"responseHeader\" : {    \"sendDate\" : \""+nowAsISO+"\",    \"requestId\" : \""+reqId+"\",  \"X-HMAC-SIGNATURE\" : \""+X_JWS_SIGNATURE+"\" } }",HttpStatus.UNAUTHORIZED);
    	    }
    	} catch (JwtException e) {
    		return new ResponseEntity<String>("{ \"code\" : \"401\", \"message\" : \"Integrity Error, JWS is invalid\" ,\"responseHeader\" : {    \"sendDate\" : \""+nowAsISO+"\",    \"requestId\" : \""+reqId+"\",  \"X-HMAC-SIGNATURE\" : \""+X_JWS_SIGNATURE+"\" } }",HttpStatus.UNAUTHORIZED);
    	}
        return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
    }
        
    static public byte[] calcHmacSha256(byte[] secretKey, byte[] message) {
        byte[] hmacSha256 = null;
        try {
          Mac mac = Mac.getInstance("HmacSHA256");
          SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, "HmacSHA256");
          mac.init(secretKeySpec);
          hmacSha256 = mac.doFinal(message);
        } catch (Exception e) {
          throw new RuntimeException("Failed to calculate hmac-sha256", e);
        }
        return hmacSha256;
      }
}
